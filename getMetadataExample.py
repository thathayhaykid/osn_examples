# File: uploadExample.py
#       In this file, we're going to look at how we can upload to the supercdms-data bucket without actually setting environment variables
#       
# By: Haythem Mansour
# Made using Python 3.6.8

import os
import boto3
import hashlib as hb
import base64

def calculateFileHash(localpath):
    '''
        Function: calculateFileHash
            Calculates the filehash of the file provided on the local disk!

        Inputs:
            - localPath (str): the local file to get the hash of!

        Outputs:
            - contents_md5 (str): the filehash of the path provided
    '''
    myHash = hb.md5(open(localpath, 'rb').read()).digest()
    contents_md5 = base64.b64encode(myHash).decode('utf-8')

    return contents_md5


if __name__ == "__main__":
    #Getting our access key & secret key through environment variables
    access_key = os.environ.get('OSN_ACCESS_KEY')
    secret_key = os.environ.get('OSN_SECRET_KEY')

    endpoint_url = 'https://ncsa.osn.xsede.org'
    bucketname = 'supercdms-data'

    #Using the boto3 s3 resource and accessing it using the access_key and secret_key, that technically are nothing because we never really set the environment variables!
    AWSResource = boto3.resource('s3', aws_access_key_id = access_key, aws_secret_access_key = secret_key, endpoint_url = endpoint_url)
    AWSClient = boto3.client('s3', aws_access_key_id = access_key, aws_secret_access_key = secret_key, endpoint_url = endpoint_url)

    #Prepare for the file to be checked
    nameOnOSN = 'TESTS/testOSNUpload.txt'

    #Get the metadata of the object we just uploaded in 'uploadExample.py'
    metadata = AWSClient.head_object(Bucket = bucketname, Key = nameOnOSN)

    #Print what we get from the bucket
    print(metadata)


    