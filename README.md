# OSN_Examples
What is OSN Examples
--------------------

osn_examples is a repository made with examples of the code i've been using to upload, list, and delete data with the OSN.

Requirements
------------
Be sure to install the needed python3 modules specified in the requirements.txt, usually this is done using pip3

Order of Examples
-----------------

Please run the examples in this order, if you choose to do so
- uploadExample.py
- getMetadataExample.py
- deleteExample.py
- getMetadataExample.py

My Results
-----------
To see my outputs, in case you don't get the same results as i did, please take a look at the screenshots i uploaded. They show all of the different outputs for each example